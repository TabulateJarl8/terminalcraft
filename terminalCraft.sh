#!/bin/sh
echo "Updating dependencies..."
python3 -m ensurepip
python3 -m pip install -r requirements.txt

# If the script is being run in a terminal, just launch the game. Otherwise, open a terminal and launch the game.
if [ -t 1 ]
then
  python3 ./terminalCraft.py
else
  xdg-terminal "python3 ./terminalCraft.py" || x-terminal-emulator -e "python3 ./terminalCraft.py" || exo-open --launch TerminalEmulator "python3 ./terminalCraft.py"
fi
